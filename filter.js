// Funtion filter to filter out
function filter(elements, cb) {
    // Initializing empty filterdArray which will be returned
    filteredArray = [];
    // Checks if elements is array or not
    if (Array.isArray(elements) && typeof cb === "function") {
        // Iterates through each elememt
        for (let index = 0; index < elements.length; index++) {
            // Checks if satisfies follwing condition
            if (cb(elements[index]) === true) {
                filteredArray.push(elements[index])
            }
        }
        return filteredArray;
    }
    // If input is not array 
    return "Give array as input";
}

// Exporting filter function such that it can be imported in other files
module.exports = { filter };
