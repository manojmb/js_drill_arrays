function map(elements, cb) {
    mappedArray = [];
    // Checks whether elements is array or not
    if (Array.isArray(elements)) {
        // Iterate through array
        for (let index = 0; index < elements.length; index++) {
            // Push mapped element to mappedArray
            mappedArray.push(cb(elements[index]));
        }
        // Return mappedArray
        return mappedArray;
    }
    // Returns if array is not input
    return "Provide array as input";
}

// Exporting map function such that it can be imported in other files
module.exports = { map }