function reduce(elements, cb, startingValue) {
    // Checks whether elements is array or not
    if (Array.isArray(elements)) {
        // Check if starting value provided or not
        if (startingValue === undefined) {
            result = elements[0]
            for (let index = 1; index < elements.length; index++) {
                result = cb(result, elements[index])
            }
        }
        else {
            result = startingValue
            for (let index = 0; index < elements.length; index++) {
                result = cb(result, elements[index])
            }
        }
        return result;
    }

}

module.exports = { reduce };
