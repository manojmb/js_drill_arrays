// Using recursion function to flatten array
function recursion(index, inputArray, outputArray) {
    // Checks if index length is greater than array length
    if (index >= inputArray.length) {
        return;
    }
    // Checks whether at index array or not
    if (Array.isArray(inputArray[index])) {
        recursion(0, inputArray[index], outputArray);
    } else {
        // If not array push into output array
        outputArray.push(inputArray[index]);
    }
    recursion(index + 1, inputArray, outputArray);
}

function flatten(elements, depth) {
    if (Array.isArray(elements)) {
        // Initialize empty outputArray to store flattened array items
        let outputArray = [];
        recursion(0, elements, outputArray);
        // Returns final flattened array
        return outputArray;
    }

}

// Exporting flatten function such that it can be imported in other files
module.exports = { flatten };
