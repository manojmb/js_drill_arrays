// Importing map function from find.js
const { map } = require("../map.js");

// Items array to check test case
const items = [1, 2, 3, 4, 5, 5];


try {
    // map function takes array and call back function as input
    result = map(items, (item) => {
        return item * 2;
    })
    console.log("\nMap problem test case");
    // Logs result on console
    console.log(result);
} catch (e) {
    console.log(e.message);
}