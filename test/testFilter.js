// Importing filter function from filter.js file
const { filter } = require("../filter.js");

// Items array to check test case
const items = [1, 2, 3, 4, 5, 5];

try {
    // filter function takes array and call back function as input
    result = filter(items, (item) => {
        return item > 3;
    });
    console.log("\nFilter problem test case");
    // Logs result on console
    console.log(result);
} catch (e) {
    console.log(e.message);
}