// Importing flatten function from find.js
const { flatten } = require("../flatten.js");

// Nested array to check test case
const nestedArray = [1, [2], [[3]], [[[4]]]];

try {
    // flatten function takes array return flattened array
    result = flatten(nestedArray);
    console.log("\nFlatten problem test cases");
    // Logs result on console
    console.log(result);
} catch (e) {
    console.log(e.message);
}