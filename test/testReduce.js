// Importing map function from find.js
const { reduce } = require("../reduce.js");

// Items array to check test case
const items = [1, 2, 3, 4, 5, 5];

try {
    // map function takes array and call back function and starting value as input
    result = reduce(items, (accumulator, currentValue) => {
        return accumulator + currentValue;
    }, 20);
    console.log("\nReduce problem test cases");
    console.log("test case 1");
    // Logs result on console
    console.log(result);
    // starting value not given
    result = reduce(items, (accumulator, currentValue) => {
        return accumulator + currentValue;
    });
    console.log("test case 2");
    // Logs result on console
    console.log(result);
} catch (e) {
    console.log(e.message);
}