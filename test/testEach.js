// Importing function each from each.js file
const { each } = require("../each.js");

// Items array to check test case
const items = [1, 2, 3, 4, 5, 5];

try {
    console.log("Each problem test cases:-\ntest1");
    // each function takes array and call back function as input
    // test 1
    each(items, (item) => {
        console.log(item);
    });
    console.log("\ntest 2");
    // test 2
    // Logs result on console
    each("help", (item) => {
        console.log(item);
    });
} catch (e) {
    // If any array encountered print message
    console.log(e.message);
}