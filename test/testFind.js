// Importing find function from find.js
const { find } = require("../find.js");

// Items array to check test case
const items = [1, 2, 3, 4, 5, 5];

try {
    console.log("\nFind problem test cases:- \ncase 1");
    // find function takes array and call back function as input
    result = find(items, (item) => {
        return item > 3;
    });
    // Logs result on console
    console.log(result);
    result = find(items, (item) => {
        return item > 33;
    });
    console.log("\ncase 2")
    console.log(result);
} catch (e) {
    console.log(e.message);
}