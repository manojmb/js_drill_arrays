function find(elements, cb) {
    // Checks whether elements is array or not
    if (Array.isArray(elements) && typeof cb === "function") {
        // Iterates through each elememt
        for (let index = 0; index < elements.length; index++) {
            // Checks if satisfies follwing condition
            if (cb(elements[index]) === true) {
                // Returns the satisfied element
                return elements[index];
            }
        }
        // If no condition satisfies
        return undefined;
    }
    // If input is not array 
    return "Give array as input";
}

// Exporting find function such that it can be imported in other files
module.exports = { find };
