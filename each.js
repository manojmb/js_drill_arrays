// Funtion each acts similar to forEach and iterates through each item
function each(elements, cb) {
    // Checks whether elements is array or not
    if (Array.isArray(elements) && typeof cb === "function") {
        // Iterates through elements
        for (let index = 0; index < elements.length; index++) {
            // Applying given function on individual elements
            cb(elements[index]);
        }
    }
    else {
        // If input is not array
        console.log("Please check if array and function is input");
    }
}

// Exporting function each so that it can be used in other files
module.exports = { each };